Source: flint-arb
Section: math
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Julien Puydt <jpuydt@debian.org>
Homepage: https://github.com/fredrik-johansson/arb
Build-Depends: debhelper (>= 11), libflint-dev (>= 2.5.2-17), libgmp-dev, libmpfr-dev
Standards-Version: 4.2.1
Vcs-Browser: https://salsa.debian.org/science-team/flint-arb
Vcs-Git: https://salsa.debian.org/science-team/flint-arb.git

Package: libflint-arb2
Section: libs
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: C library for arbitrary-precision ball arithmetic, shared library
 FLINT-ARB is a C library for high-performance arbitrary-precision
 floating-point ball (mid-rad interval) arithmetic. It supports complex
 numbers, polynomials, matrices, and evaluation of special functions, all
 with rigorous error bounding.
 .
 This package contains the shared library.

Package: libflint-arb-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, libflint-arb2 (= ${binary:Version})
Description: C library for arbitrary-precision ball arithmetic, development files
 FLINT-ARB is a C library for high-performance arbitrary-precision
 floating-point ball (mid-rad interval) arithmetic. It supports complex
 numbers, polynomials, matrices, and evaluation of special functions, all
 with rigorous error bounding.
 .
 This package contains the development files.

Package: libflint-arb-doc
Section: doc
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: C library for arbitrary-precision ball arithmetic, documentation
 FLINT-ARB is a C library for high-performance arbitrary-precision
 floating-point ball (mid-rad interval) arithmetic. It supports complex
 numbers, polynomials, matrices, and evaluation of special functions, all
 with rigorous error bounding.
 .
 This package contains the documentation.
